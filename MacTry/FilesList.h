//
//  FilesList.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MetaData.h"

@class HashableFile;

@interface FilesList : NSObject

- (NSArray<HashableFile *>*)files;
- (void)addFilesWithUrls:(NSArray<NSURL*> *)urls;
- (void)removeFile:(HashableFile*)file;
- (void)clear;

@end
