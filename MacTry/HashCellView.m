//
//  HashCellView.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/12/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "HashCellView.h"
@interface HashCellView ()

@property (strong) IBOutlet NSProgressIndicator *progressBar;

@end

@implementation HashCellView

- (void)awakeFromNib {
    self.progressBar.minValue = 0.0;
    self.progressBar.maxValue = 1.0;
}

@end
