//
//  FilesHasher.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/12/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "FilesHasher.h"
#import "HashOperation.h"

static NSInteger const kMaxCouncurrentOperation = 10;

@interface FilesHasher ()

@property(nonatomic, strong) NSOperationQueue *hashQueue;
@property(nonatomic, strong) NSArray<HashableFile *> *files;
@end

@implementation FilesHasher

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hashQueue = [NSOperationQueue new];
        self.hashQueue.maxConcurrentOperationCount = kMaxCouncurrentOperation;
    }
    return self;
}

- (void)hashFiles:(NSArray<HashableFile *> *)files {
    self.files = files;
    [files enumerateObjectsUsingBlock:^(HashableFile * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.hashProgress = 0.0;
        obj.isHashingInProgress = YES;
    }];
    for (HashableFile *file in files) {
        HashOperation *operation = [[HashOperation alloc] init];
        operation.file = file;
        operation.qualityOfService = NSQualityOfServiceUserInteractive;
        [self.hashQueue addOperation:operation];
    }
}

- (void)stopHashing {
    [self.hashQueue cancelAllOperations];
    [self.files enumerateObjectsUsingBlock:^(HashableFile * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isHashingInProgress = NO;
    }];
    self.files = nil;
}



@end
