//
//  Directory.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "FilesList.h"
#import "HashableFile.h"

@interface FilesList ()

@property(nonatomic, strong) NSMutableArray<HashableFile *> *files;

@end

@implementation FilesList

- (instancetype)init {
    self = [super init];
    if (self) {
        _files = [NSMutableArray new];
    }
    return self;
}

- (NSArray<NSURL*> *)filterOutFolders:(NSArray<NSURL*> *)urls {
    NSMutableArray *array = [NSMutableArray new];
    
    [urls enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *attributes = [obj resourceValuesForKeys:@[NSURLIsDirectoryKey] error:nil];
        BOOL isDirectory = [[attributes objectForKey:NSURLIsDirectoryKey] boolValue];
        if (!isDirectory) {
            [array addObject:obj];
        }
    }];

    return array;
}

- (void)addFilesWithUrls:(NSArray<NSURL*> *)urls {
    urls = [self filterOutFolders:urls];
    for (NSURL *fileURL in urls) {
        [_files addObject:[[HashableFile alloc]
                           initWithMetaData:[MetaData metaDataFromUrl:fileURL]]];
    }
}

- (NSArray<HashableFile *> *)files {
    return _files;
}

- (void)removeFile:(HashableFile*)file {
    [_files removeObject:file];
}

- (void)clear {
    [_files removeAllObjects];
}


@end






