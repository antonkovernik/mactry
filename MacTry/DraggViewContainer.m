//
//  DraggViewContainer.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/15/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "DraggViewContainer.h"

@interface DraggViewContainer () <NSDraggingDestination>

@end

@implementation DraggViewContainer

- (void)awakeFromNib {
    [super awakeFromNib];
    [self registerForDraggedTypes:@[NSURLPboardType, NSFilenamesPboardType]];
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    return NSDragOperationCopy;
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)sender {
    NSPasteboard *pasteboard = [sender draggingPasteboard];
    NSArray *files = [pasteboard propertyListForType:NSFilenamesPboardType];
    NSMutableArray *fileUrls = [NSMutableArray new];

    [files enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [fileUrls addObject:[NSURL fileURLWithPath:obj]];
    }];
    [self.dragDelegate draggedFiles:fileUrls];
    
    return YES;
}

@end
