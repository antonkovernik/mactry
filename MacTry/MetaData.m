//
//  MetaData.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "MetaData.h"

@interface MetaData ()

@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSDate *date;
@property(nonatomic, assign) NSUInteger size;
@property(nonatomic, strong) NSImage *icon;
@property(nonatomic, strong) NSURL *url;

@end

@implementation MetaData

- (instancetype)initWithFileUrl:(NSURL*)url name:(NSString*)name date:(NSDate*)date size:(NSInteger)size icon:(NSImage*)icon {
    self = [super init];
    if (self) {
        _url = url;
        _name = name;
        _date = date;
        _size = size;
        _icon = icon;
    }
    return self;
}

+ (instancetype)metaDataFromUrl:(NSURL *)fileURL {
        NSArray *requiredAttributes = @[NSURLLocalizedNameKey,
                                        NSURLEffectiveIconKey,
                                        NSURLContentModificationDateKey,
                                        NSURLFileSizeKey];
        NSDictionary *attributes = [fileURL resourceValuesForKeys:requiredAttributes error:nil];
    
        return [[MetaData alloc] initWithFileUrl:fileURL
                                            name:[attributes objectForKey:NSURLLocalizedNameKey]
                                            date:[attributes objectForKey:NSURLContentModificationDateKey]
                                            size:[[attributes objectForKey:NSURLFileSizeKey] integerValue]
                                            icon:[attributes objectForKey:NSURLEffectiveIconKey]];
}

@end
