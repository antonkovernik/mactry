//
//  DraggViewContainer.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/15/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol DraggViewDelegate

@required
- (void)draggedFiles:(NSArray *)files;

@end

@interface DraggViewContainer : NSView

@property (nonatomic, weak) id<DraggViewDelegate> dragDelegate;

@end
