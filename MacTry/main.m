//
//  main.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
