//
//  FilesHasher.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/12/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HashableFile;

@interface FilesHasher : NSObject

- (void)hashFiles:(NSArray<HashableFile *> *)files;
- (void)stopHashing;

@end
