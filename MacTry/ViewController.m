//
//  ViewController.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "ViewController.h"
#import "MetaData.h"
#import "FilesList.h"

#import "FilesHasher.h"
#import "FileRemover.h"
#import "DraggViewContainer.h"

typedef NS_ENUM(NSInteger, FileOperationType) {
    FileOperationTypeHash,
    FileOperationTypeRemove
};

@interface ViewController() <DraggViewDelegate>

@property (nonatomic, strong) IBOutlet NSPopUpButton *fileOperationPopUp;
@property (nonatomic, strong) IBOutlet NSArrayController *dataSource;

@property (nonatomic, strong) DraggViewContainer *view;
@property (nonatomic, strong) FilesHasher *hasher;
@property (nonatomic, strong) FileRemover *remover;
@property (nonatomic, strong) FilesList *filesList;

@end


@implementation ViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.hasher = [[FilesHasher alloc] init];
    self.remover = [[FileRemover alloc] init];
    self.filesList = [[FilesList alloc] init];
    self.view.dragDelegate = self;
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    if ([representedObject isKindOfClass:[NSArray class]]) {
        [self updateFilesListWith:representedObject];
    }
}

- (IBAction)submitButtonPressed:(NSButton *)sender {
    switch (self.fileOperationPopUp.indexOfSelectedItem) {
        case FileOperationTypeHash:
            [self calculateHashForFiles];
            break;
        case FileOperationTypeRemove:
            [self removeFiles];
            break;
        default:
            break;
    }
}

- (IBAction)clearButtonPressed:(NSButton *)sender {
    [self.filesList clear];
    self.dataSource.content = nil;
}

- (void)calculateHashForFiles {
    [self.hasher hashFiles:self.dataSource.content];
}

- (void)removeFiles {
    [self.remover removeFiles:self.dataSource.content];
    [self.filesList clear];
    self.dataSource.content = nil;
}

- (void)draggedFiles:(NSArray<NSURL*> *)files {
    [self updateFilesListWith:files];
}

- (void)updateFilesListWith:(NSArray<NSURL*> *)files {
    [self.filesList addFilesWithUrls:files];
    self.dataSource.content = self.filesList.files;

}

@end
















