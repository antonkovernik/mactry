//
//  HashableFile.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/15/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MetaData.h"

@protocol IHashableFile <IMetaData>

- (void)setIsHashingInProgress:(BOOL)isHashingInProgress;
- (BOOL)isHashingInProgress;

- (void)setHashProgress:(CGFloat)progress;
- (CGFloat)hashProgress;

- (void)setFileHash:(NSString*)fileHash;
- (NSString*)fileHash;

@end

@interface HashableFile : NSObject <IHashableFile>

- (instancetype)initWithMetaData:(MetaData*)metaData;

@end
