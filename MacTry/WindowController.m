//
//  WindowController.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "WindowController.h"

@implementation WindowController

- (IBAction)openDocument:(id)sender {
    
    NSOpenPanel *panel = [[NSOpenPanel alloc] init];
    
    panel.showsHiddenFiles = NO;
    panel.canChooseFiles = YES;
    panel.canChooseDirectories = NO;
    panel.allowsMultipleSelection = YES;
    
    [panel beginSheetModalForWindow:self.window completionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            self.contentViewController.representedObject = panel.URLs;
        }
    }];
}

@end
