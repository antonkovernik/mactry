//
//  AppDelegate.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

