//
//  HashOperation.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/12/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HashableFile.h"

@interface HashOperation : NSOperation

@property(strong, nonatomic) id<IHashableFile> file;

@end
