//
//  HashableFile.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/15/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "HashableFile.h"

@interface HashableFile ()

@property(nonatomic, strong) MetaData *metaData;
@property(nonatomic, assign) BOOL isHashingInProgress;
@property(nonatomic, assign) CGFloat hashProgress;
@property(nonatomic, strong) NSString *fileHash;

@end

@implementation HashableFile

- (instancetype)initWithMetaData:(MetaData*)metaData {
    self = [super init];
    if (self) {
        self.metaData = metaData;
        self.fileHash = @"--";
        self.hashProgress = 0.0;
        self.isHashingInProgress = NO;
    }
    return self;
}

- (NSUInteger)size {
    return self.metaData.size;
}

- (NSURL *)url {
    return self.metaData.url;
}

@end
