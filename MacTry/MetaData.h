//
//  MetaData.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/11/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol IMetaData

- (NSUInteger)size;
- (NSURL*)url;

@end

@interface MetaData : NSObject <IMetaData>

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
- (instancetype)initWithFileUrl:(NSURL*)url name:(NSString*)name date:(NSDate*)date size:(NSInteger)size icon:(NSImage*)icon;

+ (instancetype)metaDataFromUrl:(NSURL*)url;

@end
