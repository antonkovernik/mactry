//
//  HashOperation.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/12/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "HashOperation.h"
#import <CommonCrypto/CommonDigest.h>

static NSInteger const kStepsCount = 100;
static CGFloat const kStep = 0.01;
static size_t const kChunkSizeForReadingData = 4096;

@implementation HashOperation

- (void)main {
    NSString *md5Hash = nil;
    
    NSInteger fileSize = self.file.size;
    CFURLRef fileURL = CFBridgingRetain(self.file.url);
    
    CC_MD5_CTX hashObject;

    CFReadStreamRef readStream = CFReadStreamCreateWithFile(kCFAllocatorDefault, fileURL);
    if (CFReadStreamOpen(readStream)) {
        
        CGFloat progress = 0.0;
        CGFloat progressStep = fileSize / kStepsCount;
        
        CC_MD5_Init(&hashObject);
        BOOL hasMoreData = YES;
        while(hasMoreData) {
            uint8_t buffer[kChunkSizeForReadingData];
            CFIndex readBytesCount = CFReadStreamRead(readStream, (UInt8 *)buffer, (CFIndex)sizeof(buffer));
            if (readBytesCount == -1) {
                break;
            } else if (readBytesCount == 0) {
                hasMoreData = NO;
            } else {
                CC_MD5_Update(&hashObject, (const void*)buffer, (CC_LONG)readBytesCount);
            }
            if (self.isCancelled) {
                [self cancel];
                return;
            }
            progress += kChunkSizeForReadingData;
            if (progress >= progressStep) {
                progress = 0.0;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.file.hashProgress += kStep;
                });
            }
        }
        
        unsigned char digest[CC_MD5_DIGEST_LENGTH];
        CC_MD5_Final(digest, &hashObject);
        CFReadStreamClose(readStream);
        char hash[2 * sizeof(digest) + 1];
        for (size_t i = 0; i < sizeof(digest); ++i) {
            snprintf(hash + (2 * i), 3, "%02x", (int)(digest[i]));
        }
        md5Hash = [NSString stringWithUTF8String:hash];
    }
    
    if (readStream) {
        CFRelease(readStream);
    }
    if (fileURL) {
        CFRelease(fileURL);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.file.isHashingInProgress = NO;
        self.file.fileHash = md5Hash;
    });
}

- (void)cacncel {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.file.isHashingInProgress = NO;
        self.file.fileHash = @"--";
    });
}


@end
