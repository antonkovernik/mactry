    //
//  FileRemover.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/13/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "FileRemover.h"
#include <ServiceManagement/ServiceManagement.h>
#import "HelperTool.h"

static NSString *const kIsHelperInstalled = @"isHelperInstalled";
static NSInteger const kPremissionErrorCode = 513;
@interface FileRemover ()

@property (atomic, strong, readwrite) NSXPCConnection *helperToolConnection;

@end

@implementation FileRemover

- (void)removeFiles:(NSMutableArray<IHashableFile>*)files {
    for (HashableFile *file in files) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtURL:file.url error:&error];
        if (error && error.code == kPremissionErrorCode) {
                [self installHelperIfNeeded];
                [self connectToHelperTool];
                [self removeFileWithUrl:file.url];
        }
    }
}

- (void)installHelperIfNeeded {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kIsHelperInstalled]) {
        return;
    }
    AuthorizationRef authRef;
    OSStatus status = AuthorizationCreate(NULL, kAuthorizationEmptyEnvironment, kAuthorizationFlagDefaults, &authRef);
    if (status != errAuthorizationSuccess) {
        assert(NO);
        authRef = NULL;
    }
    
    BOOL result = NO;
    NSError * error = nil;
    
    AuthorizationItem authItem		= { kSMRightBlessPrivilegedHelper, 0, NULL, 0 };
    AuthorizationRights authRights	= { 1, &authItem };
    AuthorizationFlags flags		=	kAuthorizationFlagDefaults				|
                                        kAuthorizationFlagInteractionAllowed	|
                                        kAuthorizationFlagPreAuthorize			|
                                        kAuthorizationFlagExtendRights;
    status = AuthorizationCopyRights(authRef, &authRights, kAuthorizationEmptyEnvironment, flags, NULL);
    if (status != errAuthorizationSuccess) {
        error = [NSError errorWithDomain:NSOSStatusErrorDomain code:status userInfo:nil];
    } else {
        CFErrorRef  cfError;
        result = (BOOL) SMJobBless(kSMDomainSystemLaunchd, (CFStringRef)CFSTR("com.MacTry.HelperTool"), authRef, &cfError);
        if (!result) {
            error = CFBridgingRelease(cfError);
            NSLog(@"%@", error.localizedDescription);
        } else {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsHelperInstalled];
        }
    }
}



- (void)connectToHelperTool {
    assert([NSThread isMainThread]);
    if (self.helperToolConnection == nil) {
        self.helperToolConnection = [[NSXPCConnection alloc] initWithMachServiceName:kHelperToolMachServiceName options:NSXPCConnectionPrivileged];
        self.helperToolConnection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(HelperToolProtocol)];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
        self.helperToolConnection.invalidationHandler = ^{
            self.helperToolConnection.invalidationHandler = nil;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.helperToolConnection = nil;
            }];
        };
#pragma clang diagnostic pop
        [self.helperToolConnection resume];
    }
}

- (void)removeFileWithUrl:(NSURL*)url {
    [[self.helperToolConnection remoteObjectProxyWithErrorHandler:^(NSError * _Nonnull error) {
        NSLog(@"There was an error while sending a message to helper %@", error.localizedDescription);
    }] removeFilePrivelegedWithUrl:url];
}


@end
