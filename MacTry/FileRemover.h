//
//  FileRemover.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/13/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HashableFile.h"

@interface FileRemover : NSObject

- (void)removeFiles:(NSArray<HashableFile*>*)files;

@end
