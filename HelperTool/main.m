//
//  main.m
//  HelperTool
//
//  Created by Anton Kovernyk on 1/13/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <syslog.h>


#import "HelperTool.h"

int main(int argc, char **argv)
{
    #pragma unused(argc)
    #pragma unused(argv)

    @autoreleasepool {
        HelperTool *tool = [[HelperTool alloc] init];
        [tool run];
    }
    return EXIT_FAILURE;
}
