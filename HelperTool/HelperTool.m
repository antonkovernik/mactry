//
//  HelperTool.m
//  MacTry
//
//  Created by Anton Kovernyk on 1/13/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import "HelperTool.h"


@interface HelperTool () <NSXPCListenerDelegate, HelperToolProtocol>

@property (atomic, strong, readwrite) NSXPCListener *    listener;

@end

@implementation HelperTool

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.listener = [[NSXPCListener alloc] initWithMachServiceName:kHelperToolMachServiceName];
        self.listener.delegate = self;
    }
    return self;
}

- (void)run {

    [self.listener resume];

    [[NSRunLoop currentRunLoop] run];
}

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection {
    assert(listener == self.listener);
    #pragma unused(listener)
    assert(newConnection != nil);
    
    newConnection.exportedInterface = [NSXPCInterface interfaceWithProtocol:@protocol(HelperToolProtocol)];
    newConnection.exportedObject = self;
    [newConnection resume];
    
    return YES;
}

- (void)removeFilePrivelegedWithUrl:(NSURL*)url {
    remove(url.path.UTF8String);
}

@end
