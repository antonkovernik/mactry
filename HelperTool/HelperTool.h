//
//  HelperTool.h
//  MacTry
//
//  Created by Anton Kovernyk on 1/13/17.
//  Copyright © 2017 Anton Kovernyk. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kHelperToolMachServiceName @"com.MacTry.HelperTool"


@protocol HelperToolProtocol <NSObject>

@required

- (void)removeFilePrivelegedWithUrl:(NSURL*)url;

@end

@interface HelperTool : NSObject

- (void)run;

@end
