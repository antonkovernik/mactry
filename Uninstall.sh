#! /bin/sh
# This uninstalls everything installed by the sample.

sudo launchctl unload /Library/LaunchDaemons/com.MacTry.HelperTool.plist
sudo rm /Library/LaunchDaemons/com.MacTry.HelperTool.plist
sudo rm /Library/PrivilegedHelperTools/com.MacTry.HelperTool

defaults delete com.MacTry.App isHelperInstalled
